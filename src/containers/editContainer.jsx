import { connect } from 'react-redux';
import * as action from '../actions/editAction';
import todoEdit from '../components/TodoEdit';


const mapStateToProps = (state) => {
    return state
}

const mapDispatchToProps = dispatch => {
    return {
        editTask: (newlist, usr) => dispatch(action.editTask(newlist, usr)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(todoEdit)