import { connect } from 'react-redux';
import * as action from './../actions/loginAction';
import login from './../components/Login';


const mapStateToProps = (state) => {
    return state
}

const mapDispatchToProps = dispatch => {
    return {
        login: (id, pass) => dispatch(action.checkLogin(id, pass)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(login)