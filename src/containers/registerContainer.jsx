import { connect } from 'react-redux';
import * as action from '../actions/regisetrAction';
import todoRegister from '../components/TodoRegister';


const mapStateToProps = (state) => {
    return state
}

const mapDispatchToProps = dispatch => {
    return {
        addTask: (newlist, usr) => dispatch(action.addTask(newlist, usr)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(todoRegister)