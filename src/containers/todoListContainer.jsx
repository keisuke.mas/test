import { connect } from 'react-redux';
import * as action from '../actions/todoListAction';
import todoList from '../components/TodoList';


const mapStateToProps = (state) => {
    return state
}

const mapDispatchToProps = dispatch => {
    return {
        logout: () => dispatch(action.logout()),
        deleteTask: (afterDeleteList, usr) => dispatch(action.deleteTask(afterDeleteList, usr)),
        getWeather: () => dispatch(action.getWeather())
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(todoList)