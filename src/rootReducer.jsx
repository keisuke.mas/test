import { combineReducers } from 'redux'
import taskReducer from './reducers/taskReducer'
import userReducer from './reducers/userReducer'
import apiReducer from './reducers/apiReducer'

const rootReducer = combineReducers({
    taskReducer,
    userReducer,
    apiReducer
});

export default rootReducer;