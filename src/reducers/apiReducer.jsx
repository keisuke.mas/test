const initialState = {
    items: {}
}

const apiReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_POSTS_REQUEST':
            return Object.assign({}, state, { isFetching: true }, { items: {} })
        case 'GET_POSTS_SUCCESS':
            return Object.assign({}, state, { isFetching: false }, { items: action.posts }, { status: "OK" })
        case 'GET_POSTS_FAILURE':
            return Object.assign({}, state, { isFetching: false }, { status: action.error })
        default:
            return state;
    }
};

export default apiReducer