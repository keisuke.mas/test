const initialState = {
        aaaaaa: [],
        bbbbbb: [],
        cccccc: []
}

const taskReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADDTASK':
            return Object.assign({}, state, action);
        case 'EDITTASK':
            return Object.assign({}, state, action);
        case 'DELETETASK':
            return Object.assign({}, state, action);
        default:
            return state;
    }
};

export default taskReducer