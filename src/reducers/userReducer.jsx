const initialState = {
    userInfo: {}
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'LOGINOK':
            return Object.assign({}, state, action);
        case 'LOGINNG':
            return Object.assign({}, state, action);
        case 'LOGOUT':
            return Object.assign({}, state, { type: action.type }, { userInfo: {} });
        default:
            return state;
    }
};

export default userReducer