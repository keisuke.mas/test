import { createStore, compose, applyMiddleware } from 'redux';
import reducer from './rootReducer'
import persistState from "redux-localstorage";
import thunk from 'redux-thunk';

export const store = createStore(
    reducer,
    compose(
        //storeの拡張、非同期処理のthunkを使用
        applyMiddleware(thunk),
        //ローカルストレージに保存
        persistState(),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    ),
);