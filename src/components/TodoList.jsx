import React, { Component } from "react";
import './../css/App.css';
import { animateScroll as scroll } from "react-scroll"

class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isScroll: false
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.scrollDisplay)
  }

  componentWillMount() {
    this.props.getWeather()
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps !== this.props.id
  };

  //DOMの破棄直前に、登録していたイベントを削除して、メモリリークを防ぐ（？）
  componentWillUnmount(){
    window.removeEventListener('scroll', this.scrollDisplay)
  }

  //スクロールがなくなった時、isScrollをfalseにして、ボタンを非表示
  checkScroll = () => {
    const browserH =  window.parent.screen.height;
    const contentsH = document.getElementById("cont").scrollHeight
    if((browserH - contentsH) >= 0){
      this.setState({ isScroll: false })
    }
  }

  scrollDisplay = () => {
    if (!this.state.isScroll) {
      this.setState({ isScroll: true })
    }
  }

  moveRegister = () => {
    this.props.history.push("/todo/register");
  }

  editTask = (elm) => {
    this.props.history.push({
      pathname: "/todo/detail/" + elm.id,
      state: { changeTask: elm }
    })
  };

  deleteTask = (elm) => {
    let result = window.confirm("本当に削除してもよろしいですか")
    if (result) {
      const afterDeleteList = this.props.taskReducer[this.props.userReducer.userInfo.userId].filter(val => val.id !== elm.id)
      this.props.deleteTask(afterDeleteList, this.props.userReducer.userInfo.userId)
      this.checkScroll()
    } else {
      return
    }
  };

  logout = () => {
    this.props.logout()
    this.props.history.push()
  };

  scrollToTop = () => {
    scroll.scrollToTop();
  }

  render() {
    const headerInfo = <header className="App-header">
      <div>
        今日の東京の天気は、「{this.props.apiReducer.items.description}」
      </div>
      {this.props.userReducer.userInfo.userName}さんのTODO一覧
      </header>
    const addButton = <div><input type="button" value="タスク追加" onClick={this.moveRegister} /></div>

    const displayList = this.props.taskReducer[this.props.userReducer.userInfo.userId];

    if (!displayList) {
      return (
        <div className="App">
          {headerInfo}
          <input type="button" value="ログアウト" onClick={this.logout} />
          <div>タスクはありません</div>
          {addButton}
        </div>
      );
    } else {
      //コンポーネント化
      const task = displayList.map(elm => (
        <li className="task" key={elm.id} id={elm.id}>
          <div>登録日：{elm.registDate}</div>
          <div>期限日：{elm.dueDate}</div>
          <div>内容：{elm.detail}</div>
          <div>
            <input type="button" value="変更" onClick={e => this.editTask(elm)} />
            <input type="button" value="削除" onClick={e => this.deleteTask(elm)} />
          </div>
        </li>
      ));
      return (
        <div id="cont">
          {headerInfo}
          <div className="Logout">
            <input className="LogoutButton" type="button" id="logoutButton" value="ログアウト" onClick={this.logout} />
          </div>
          {addButton}
          <ul id="tasks">{task}</ul>
          {this.state.isScroll && (<div className="parentToTop"><input className="toTop" type="button" value="to-top" onClick={this.scrollToTop}/></div>)}
        </div>
      );
    }
  }
}

export default TodoList;
