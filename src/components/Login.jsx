import React, { Component } from "react";
import './../css/App.css';
import { store } from './../store';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: "",
      password: "",
      loginErrorCount: 0,
      disabled: false
    };
  }

  componentDidMount() {
    if (localStorage.getItem('redux')) {
      const localDate = JSON.parse(localStorage.getItem('redux'))
      const authUser = localDate.userReducer.userInfo
      if (Object.keys(authUser).length) {
        this.props.history.push("/todo");
      }
    }
  }

  componentDidUpdate() {
    if (this.state.loginErrorCount >= 10 && !this.state.disabled) {
      alert(
        "ログインエラーの回数が10回を超えました\nログイン機能をロックします"
      );
      this.setState({ disabled: true });
    }
  }

  setUserId = e => {
    this.setState({ userId: e.target.value });
  };

  setPassword = e => {
    this.setState({ password: e.target.value });
  };

  //ログイン処理
  login = () => {
    if (this.valiCheck(this.state.userId, this.state.password)) {
      return;
    }
    this.props.login(this.state.userId, this.state.password);
    if (store.getState().userReducer.type === 'LOGINOK') {
      this.props.history.push("/todo");
    } else {
      alert("ユーザーID、またはパスワードが間違っています")
      this.setState({ loginErrorCount: this.state.loginErrorCount + 1 });
    }
  };

  //バリデーションチェック
  valiCheck = () => {
    if (!this.state.userId || !this.state.password) {
      if (this.state.userId) {
        alert("パスワードが未入力です");
        this.refs.password.focus();
      } else if (this.state.password) {
        alert("ユーザーIDが未入力です");
        this.refs.userId.focus();
      } else {
        alert("ユーザーID、及びパスワードが未入力です");
        this.refs.userId.focus();
      }
      this.setState({ loginErrorCount: this.state.loginErrorCount + 1 });
      return true;
    }
    return false;
  }


  render() {
    return (
      <div className="App">
        <header className="App-header">
          ログイン画面
      </header>
        <div>
          <div>
            <label htmlFor="userId">
              ユーザーID :
            <input
                type="text"
                ref="userId"
                maxLength="32"
                onChange={this.setUserId}
              />
            </label>
          </div>
          <div>
            <label htmlFor="password">
              パスワード :
            <input
                type="text"
                ref="password"
                maxLength="50"
                onChange={this.setPassword}
              />
            </label>
          </div>
          <div>
            <input
              type="button"
              value="ログイン"
              onClick={this.login}
              disabled={this.state.disabled}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
