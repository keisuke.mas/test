import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Login from "./../containers/loginContainer";
import TodoList from "./../containers/todoListContainer";
import TodoRegister from "./../containers/registerContainer";
import TodoEdit from "./../containers/editContainer";
import NotFound from "./404";
import Auth from "./Auth";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Login} />
            <Auth>
              <Switch>
                <Route exact path="/todo" component={TodoList} />
                <Route path="/todo/register" component={TodoRegister} />
                <Route path="/todo/detail/:todoId" component={TodoEdit} />
                <Route component={NotFound} />
              </Switch>
            </Auth>
        </Switch>
      </BrowserRouter>
    )
  }
}

export default App;