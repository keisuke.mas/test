import React, { Component } from "react";
import './../css/App.css';
import { Link } from 'react-router-dom';
import moment from 'moment';

class TodoRegister extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dueDate: "",
      detail: ""
    };
  }

  setDuedate = e => {
    this.setState({
      dueDate: e.target.value
    });
  };

  setDetailText = e => {
    this.setState({
      detail: e.target.value
    });
  };

  //本日日付を「YYYY-MM-DD」のフォーマットで取得
  getToday = () => {
    const now = moment();
    return now.format("YYYY-MM-DD")
  }

  //バリデーションチェック
  chackValid = () => {
    if (!this.state.dueDate) {
      alert("期限日を正確に入力してください")
      this.refs.dueDate.focus()
      return false
    }
    const today = this.getToday()
    if (this.state.dueDate < today) {
      alert("期限日には未来日を選択してください")
      this.refs.dueDate.focus()
      return false
    }
    if (!this.state.detail) {
      alert("内容が未入力です")
      this.refs.detail.focus()
      return false
    }
    return true
  }

  //タスクを追加して、リストを更新する
  addTask = () => {
    const valid = this.chackValid()
    if (valid) {
      //登録日
      const today = this.getToday()
      //ID
      const todoId = new Date().toString();
      const task = {
        id: todoId,
        userId: this.props.userReducer.userInfo.userId,
        detail: this.state.detail,
        dueDate: this.state.dueDate,
        registDate: today
      }
      const newList = this.props.taskReducer[this.props.userReducer.userInfo.userId].concat(task)
      this.props.addTask(newList, this.props.userReducer.userInfo.userId)
      this.props.history.push('/todo')
    } else {
      return
    }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          {this.props.userReducer.userInfo.userName}:
          登録フォーム
      </header>
        <div>
          <div>
            <label htmlFor="dueDate">
              期限日 :
                <input
                type="date"
                onChange={this.setDuedate}
                value={this.state.dueDate}
                ref="dueDate"
              />
            </label>
          </div>
          <div>
            <label htmlFor="detail">
              内容 :
                <textarea
                name="textarea"
                onChange={this.setDetailText}
                value={this.state.detail}
                ref="detail"
                maxLength="50"
              />
            </label>
          </div>
        </div>
        <div>
          <input type="button" value="登録" onClick={this.addTask} />
          <div><Link to='/todo'>＜＜戻る</Link></div>
        </div>
      </div>
    );
  }
}

export default TodoRegister;
