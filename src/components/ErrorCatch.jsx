import React, { Component } from "react";

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
      error: "",
      info: ""
    }
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true, error: error, info: info })
    // alert(error.toString())
    // alert(JSON.stringify(info))
  }

  render() {
    if (this.state.hasError) {
      return <div><h1>Something went wrong.</h1><p>{this.state.info.componentStack}</p><p>{this.state.error.toString()}</p></div>
    }
    return this.props.children
  }
}

export default ErrorBoundary;
