import React from "react";
import { Redirect } from "react-router-dom";

//ログイン中なら指定ページ、そうでないならログインページに遷移
const Auth = (props) => {
  if (!localStorage.getItem('redux')) {
    return <Redirect to={'/'} />
  } else {
    const localDate = JSON.parse(localStorage.getItem('redux'))
    const authUser = localDate.userReducer.userInfo
    if (!Object.keys(authUser).length) {
      return <Redirect to={'/'} />
    } else {
      return props.children
    }
  }
}

export default Auth