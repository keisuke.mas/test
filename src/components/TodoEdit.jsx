import React, { Component } from "react";
import './../css/App.css';
import { Link } from 'react-router-dom';
import moment from 'moment';

class TodoEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      detail: "",
      dueDate: "",
    };
  }

  //遷移元からタスク情報を引き継ぐ
  componentDidMount() {
    this.setState({
      dueDate: this.props.location.state.changeTask.dueDate,
      detail: this.props.location.state.changeTask.detail
    })
  }

  setDuedate = e => {
    this.setState({
      dueDate: e.target.value
    });
  };

  setDetailText = e => {
    this.setState({
      detail: e.target.value
    });
  };

  //バリデーションチェック
  chackValid = () => {
    if (!this.state.dueDate) {
      alert("期限日を正確に入力してください")
      this.refs.dueDate.focus()
      return false
    }
    const now = moment();
    const today = now.format("YYYY-MM-DD")
    if (this.state.dueDate < today) {
      alert("期限日には未来日を選択してください")
      this.refs.dueDate.focus()
      return false
    }
    if (!this.state.detail) {
      alert("内容が未入力です")
      this.refs.detail.focus()
      return false
    }
    return true
  }

  //編集確定処理
  comfirmEdit = () => {
    const result = this.chackValid()
    if (result) {
      //現在のtodoList情報をmapで展開し、Object.assignで編集対象のタスクのみ更新する
      const newList = this.props.taskReducer[this.props.userReducer.userInfo.userId].map(task => {
        if (task.id === this.props.location.state.changeTask.id) {
          return Object.assign({}, task, this.state)
        } else {
          return task
        }
      })
      this.props.editTask(newList, this.props.userReducer.userInfo.userId)
      this.props.history.push('/todo')
    } else {
      return
    }
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          {this.props.userReducer.userInfo.userName}:
          編集フォーム
      </header>
        <div>
          <div>
            <label htmlFor="dueDate">
              期限日 :
                <input
                type="date"
                onChange={this.setDuedate}
                value={this.state.dueDate}
                ref="dueDate"
              />
            </label>
          </div>
          <div>
            <label htmlFor="detail">
              内容 :
                <textarea
                name="textarea"
                onChange={this.setDetailText}
                value={this.state.detail}
                ref="detail"
                maxLength="50"
              />
            </label>
          </div>
        </div>
        <div>
          <input type="button" value="更新" onClick={this.comfirmEdit} />
          <div><Link to='/todo'>＜＜戻る</Link></div>
        </div>
      </div>
    );
  }
}

export default TodoEdit;
