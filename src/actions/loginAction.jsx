//登録ユーザー情報
const userList =
    [{ userId: "aaaaaa", userName: "テスト太郎", password: "aaaaaa" },
    { userId: "bbbbbb", userName: "テスト次郎", password: "bbbbbb" },
    { userId: "cccccc", userName: "テスト三郎", password: "cccccc" }]

//ログインユーザーの確認
export const checkLogin = (id, pass) => {
    for (var index of userList) {
        if (index.userId === id && index.password === pass) {
            return {
                type: "LOGINOK",
                userInfo:
                    { userId: index.userId, userName: index.userName, password: index.password }
            }
        }
    }
    return {
        type: "LOGINNG",
        userInfo: {},
    }
}