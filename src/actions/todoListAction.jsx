import axios from 'axios'

export const logout = () => {
    return {
        type: "LOGOUT",
    }
}

export const deleteTask = (afterDeleteList, usr) => {
    return {
        type: "DELETETASK",
        [usr]: afterDeleteList
    }
}

//ここから↓API呼び出しのテスト
export const GET_POSTS_REQUEST = 'GET_POSTS_REQUEST'
const getPostsRequest = () => {
    return {
        type: GET_POSTS_REQUEST
    }
}

export const GET_POSTS_SUCCESS = 'GET_POSTS_SUCCESS'
const getPostsSuccess = (json) => {
    return {
        type: GET_POSTS_SUCCESS,
        posts: json
    }
}

export const GET_POSTS_FAILURE = 'GET_POSTS_FAILURE'
const getPostsFailure = (error) => {
    return {
        type: GET_POSTS_FAILURE,
        error
    }
}

export const getWeather = () => {
    return (dispatch) => {
        dispatch(getPostsRequest())
        return axios.get(`http://api.openweathermap.org/data/2.5/weather?id=1850147&lang=ja&APPID=a7f23e85ea36572486078ddbf4de20a9`)
            .then(res =>
                dispatch(getPostsSuccess(res.data.weather[0])))
            .catch(err =>
                dispatch(getPostsFailure(err))
            )
    }
}
